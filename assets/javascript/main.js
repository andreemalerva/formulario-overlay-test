//Selecciona todos los input
const $inputList = document.querySelectorAll('input');
//Seleccciona el elemento que contiene el id overlay
const $overlay = document.querySelector('#overlay');
//recorre todos los input para en ellos escuchar el evento focus (y escucha al evento llamado focus) y blur (escucha también al evento llamado blur);
$inputList.forEach($input => {
        $input.addEventListener('focus', focus);
        $input.addEventListener('blur', blur);
    })
    //aquí vamos escuchar el evento click y posteriormente pasaremos en evento
$overlay.addEventListener('click', (event) => {
        //veremos a través de la consola que efecto es que está si el focus o el blur
        console.log(event);
        //Eliminará la clase is-active del id overlay
        $overlay.classList.remove('is-active');
        //Toma las cordenadas del canvas para ver si existe algun input
        const $maybeIsInput = document.elementFromPoint(event.clientX, event.clientY);
        if ($maybeIsInput.tagName === 'INPUT') {
            $maybeIsInput.focus(); //pasara a la funcion de focus cuando el objeto encontrado sea un input.
        }
    })
    //En esta función recibimos el evento y posteriormente, 
    //le agregamos la clase is-active y le agregamos 
    //la clase también al elemento que tenga el id overlay.
function focus(event) {
    console.log('focus');
    event.target.classList.add('is-active');
    $overlay.classList.add('is-active');
}
//En esta funcion removemos la clase cada vez que exista un blur.
function blur(event) {
    console.log('blur');
    event.target.classList.remove('is-active');
    //$overlay.classList.remove('is-active');
}