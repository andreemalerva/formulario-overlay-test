# Acerca de mí
¡Hola! 

Soy Andree, este proyecto fue hecho basado en el tutorial de [Leonidas Esteban](https://www.youtube.com/watch?v=7RNzkzhPDO0).

Trabajemos juntos, te dejo por acá mi contacto.
```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

El proyecto/tutorial consiste en poner el efecto overlay a un formulario a travez de CSS y Javascript.

El propósito aquí fue lograr el overlay, para ello se utilizo la etiqueta form, con dos inputs y un botón, a cada uno se le agrego un id y su respectivo name, así como también se agrego un div con la clase overlay.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://formulario-overlay.netlify.app)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA
